package io.swagger.api;

import io.swagger.model.*;
import io.swagger.api.DemoApiService;
import io.swagger.api.factories.DemoApiServiceFactory;

import io.swagger.annotations.ApiParam;

import com.sun.jersey.multipart.FormDataParam;

import io.swagger.model.Demo;
import io.swagger.model.Error;

import java.util.List;
import io.swagger.api.NotFoundException;

import java.io.InputStream;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.*;

@Path("/demo")

@Produces({ "application/json" })
@io.swagger.annotations.Api(description = "the demo API")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JaxRSServerCodegen", date = "2016-01-13T05:50:23.572-08:00")
public class DemoApi  {
   private final DemoApiService delegate = DemoApiServiceFactory.getDemoApi();

    @GET
    
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "demo Type", notes = "This endponits for get the specified demo resource", response = Demo.class, responseContainer = "List", tags={  })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 200, message = "An array of demo", response = Demo.class, responseContainer = "List"),
        
        @io.swagger.annotations.ApiResponse(code = 200, message = "Unexpected error", response = Demo.class, responseContainer = "List") })

    public Response demoGet(@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.demoGet(securityContext);
    }
    @POST
    
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "demo Type", notes = "This endponits for create a new demo resource", response = Demo.class, tags={  })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 200, message = "description about created resource of demo", response = Demo.class),
        
        @io.swagger.annotations.ApiResponse(code = 200, message = "Unexpected error", response = Demo.class) })

    public Response demoPost(@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.demoPost(securityContext);
    }
    @GET
    @Path("/{demo_id}")
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "demo Type", notes = "This endponits for get the specified demo resource", response = Demo.class, tags={  })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 200, message = "description about created resource of demo", response = Demo.class),
        
        @io.swagger.annotations.ApiResponse(code = 200, message = "Unexpected error", response = Demo.class) })

    public Response demoDemoIdGet(@ApiParam(value = "Fetch resource id of the demo",required=true) @PathParam("demo_id") String demoId,@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.demoDemoIdGet(demoId,securityContext);
    }
    @PUT
    @Path("/{demo_id}")
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "demo Type", notes = "This endponits for update the specified demo resource", response = Demo.class, tags={  })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 200, message = "description about updated resource of demo", response = Demo.class),
        
        @io.swagger.annotations.ApiResponse(code = 200, message = "Unexpected error", response = Demo.class) })

    public Response demoDemoIdPut(@ApiParam(value = "Update resource id of the demo",required=true) @PathParam("demo_id") String demoId,@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.demoDemoIdPut(demoId,securityContext);
    }
    @DELETE
    @Path("/{demo_id}")
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "demo Type", notes = "This endponits for delete the specified demo resource", response = Void.class, tags={  })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 204, message = "Resource deleted", response = Void.class),
        
        @io.swagger.annotations.ApiResponse(code = 200, message = "Unexpected error", response = Void.class) })

    public Response demoDemoIdDelete(@ApiParam(value = "Delete resource id of the demo",required=true) @PathParam("demo_id") String demoId,@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.demoDemoIdDelete(demoId,securityContext);
    }
}
