package io.swagger.api;

import io.swagger.api.*;
import io.swagger.model.*;

import com.sun.jersey.multipart.FormDataParam;

import io.swagger.model.Demo;
import io.swagger.model.Error;

import java.util.List;
import io.swagger.api.NotFoundException;

import java.io.InputStream;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JaxRSServerCodegen", date = "2016-01-13T05:50:23.572-08:00")
public abstract class DemoApiService {
  
      public abstract Response demoGet(SecurityContext securityContext)
      throws NotFoundException;
  
      public abstract Response demoPost(SecurityContext securityContext)
      throws NotFoundException;
  
      public abstract Response demoDemoIdGet(String demoId,SecurityContext securityContext)
      throws NotFoundException;
  
      public abstract Response demoDemoIdPut(String demoId,SecurityContext securityContext)
      throws NotFoundException;
  
      public abstract Response demoDemoIdDelete(String demoId,SecurityContext securityContext)
      throws NotFoundException;
  
}
