package io.swagger.api.factories;

import io.swagger.api.DemoApiService;
import io.swagger.api.impl.DemoApiServiceImpl;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JaxRSServerCodegen", date = "2016-01-13T05:50:23.572-08:00")
public class DemoApiServiceFactory {

   private final static DemoApiService service = new DemoApiServiceImpl();

   public static DemoApiService getDemoApi()
   {
      return service;
   }
}
